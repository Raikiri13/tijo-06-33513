package pl.edu.pwsztar.domain.dto;

import pl.edu.pwsztar.domain.mapper.MoviesCounterMapper;

import java.io.Serializable;

public class MoviesCounterDto implements Serializable {

    long counter;

    public MoviesCounterDto(){

    }

    public long getCounter() {
        return counter;
    }

    public void setCounter(long counter) {
        this.counter = counter;
    }

    @Override
    public String toString() {
        return "MoviesCounterDto{" +
                "counter=" + counter +
                '}';
    }

}
