package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.MoviesCounterDto;

@Component
public class MoviesCounterMapper {

    public MoviesCounterDto counterToCounterDto(long counter){

        MoviesCounterDto moviesCounterDto = new MoviesCounterDto();
        moviesCounterDto.setCounter(counter);
        return moviesCounterDto;
    }

}
